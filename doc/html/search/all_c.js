var searchData=
[
  ['next_5fgeneration',['next_generation',['../classGA.html#ac2a2a34d79764fc9a46e52cd15d31788',1,'GA']]],
  ['nonuniform_5fmutation',['nonuniform_mutation',['../mutation_8cpp.html#ae6777cedce194a242044407f4f4348fe',1,'nonuniform_mutation(std::vector&lt; REAL_ &gt; genome, std::size_t time, std::size_t generations, std::size_t order, REAL_ a, REAL_ b):&#160;mutation.cpp'],['../gaim_8h.html#a9da463a966f021156935b116f113b027',1,'nonuniform_mutation(std::vector&lt; REAL_ &gt;, std::size_t, std::size_t, std::size_t, REAL_, REAL_):&#160;mutation.cpp']]],
  ['num_5fimmigrants',['num_immigrants',['../structparameter__im.html#a23c8aa9f8f7c618113640f1d497645cf',1,'parameter_im::num_immigrants()'],['../classIM.html#a63b324a454c4c57d5438b89f0bfec051',1,'IM::num_immigrants()']]],
  ['num_5fislands',['num_islands',['../structparameter__im.html#a922dd1fedfeedfe9281ac9b521aa8208',1,'parameter_im::num_islands()'],['../classIM.html#a0544287f3561fb2a6288ab3ee0215d4a',1,'IM::num_islands()']]],
  ['num_5foffsprings',['num_offsprings',['../structparameter__ga.html#abaa5369cab51b8d7832e07897ae25a5e',1,'parameter_ga']]],
  ['num_5freplacement',['num_replacement',['../structparameter__ga.html#aaf0d11846c4c999777935576c8ded7ef',1,'parameter_ga']]]
];
