var indexSectionsWithContent =
{
  0: "_abcdefghilmnoprstuw~",
  1: "_gip",
  2: "abcgilmpst",
  3: "_acdefgimnoprstu~",
  4: "abcefghilmnoprstw",
  5: "_gip",
  6: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "related"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Friends"
};

