var files =
[
    [ "auxiliary_funs.cpp", "auxiliary__funs_8cpp.html", "auxiliary__funs_8cpp" ],
    [ "barrier.h", "barrier_8h.html", "barrier_8h" ],
    [ "crossover.cpp", "crossover_8cpp.html", "crossover_8cpp" ],
    [ "gaim.h", "gaim_8h.html", "gaim_8h" ],
    [ "genetic_alg.cpp", "genetic__alg_8cpp.html", null ],
    [ "island_ga.cpp", "island__ga_8cpp.html", "island__ga_8cpp" ],
    [ "logging.cpp", "logging_8cpp.html", "logging_8cpp" ],
    [ "mutation.cpp", "mutation_8cpp.html", "mutation_8cpp" ],
    [ "parallel_ga.cpp", "parallel__ga_8cpp.html", "parallel__ga_8cpp" ],
    [ "parameters.cpp", "parameters_8cpp.html", "parameters_8cpp" ],
    [ "selection.cpp", "selection_8cpp.html", "selection_8cpp" ],
    [ "test_functions.cpp", "test__functions_8cpp.html", "test__functions_8cpp" ]
];