var classIM =
[
    [ "IM", "classIM.html#a7899c310524ff324c00a92e4ad3806c8", null ],
    [ "~IM", "classIM.html#a450bc003d188ce2f3ff72b83ce78abfb", null ],
    [ "evolve_island", "classIM.html#a50aa7da10d1a7902b3d94e80f7be0036", null ],
    [ "move_immigrants", "classIM.html#a95d01ae6295ca878be06817be8448501", null ],
    [ "read_connectivity_graph", "classIM.html#a143b67a3a3d436e57311ac048cb76696", null ],
    [ "run_islands", "classIM.html#a2113da7f6cacf768990ddc8c5a374526", null ],
    [ "select_ind2migrate", "classIM.html#a15fff93906932ec0bdc0d97c9181675f", null ],
    [ "a", "classIM.html#a4fc85a7e303c58f74399744a58c4dfe4", null ],
    [ "adj_list", "classIM.html#a5f4e569a338e52929a0488c036165832", null ],
    [ "b", "classIM.html#a33dbe595c898c6a4205a4a3873a75167", null ],
    [ "generations", "classIM.html#aee2c2c334f5e95c2c7c1b8a3db811410", null ],
    [ "island", "classIM.html#abbd9daac5118d5a6a3a0f1618bb69e71", null ],
    [ "migration_interval", "classIM.html#a2a4461a34fa9de6d4ea7c0dbe5653f55", null ],
    [ "migration_steps", "classIM.html#af10bdb5e2efb6d30352defe63434d98b", null ],
    [ "num_immigrants", "classIM.html#a63b324a454c4c57d5438b89f0bfec051", null ],
    [ "num_islands", "classIM.html#a0544287f3561fb2a6288ab3ee0215d4a", null ]
];