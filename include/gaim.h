/* Header file "genetic_alg.h" of GAIM package 
 * Copyright (C) 2019  Georgios Detorakis (gdetor@protonmail.com)
 *                     Andrew Burton (ajburton@uci.edu)
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. */

// $Id$
/**
 * @file genetic_alg.h
 * Header file of GA and IM classes (and their methods), comprising the main
 * external interface.
 */
// $Log$
#ifndef GENETIC_ALG_H
#define GENETIC_ALG_H

#include <iostream>
#include <vector>
#include <random>
#include <string>
#include <sstream>
#include <algorithm>
#include <iomanip>
#include <cstdlib>
#include <fstream>
#include <libconfig.h++>
#include <map>
#include <thread>
#include <mutex>

#include<sys/stat.h>

#define REAL_ float

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_RESET   "\x1b[0m"


#define XORSWAP(a, b)                                                         \
  ((&(a) == &(b)) ? (a)                                                       \
                  : ((a) ^= (b), (b) ^= (a),                                  \
                     (a) ^= (b))) /* checks that the addresses of a and b are \
                                     different before XOR-ing */


#define check(id, name) {if(id) { \
                    printf("%s ", name); \
                    printf(ANSI_COLOR_RED "TEST FAILED\n" ANSI_COLOR_RESET); \
                    exit(-1);} \
                   else { \
                       printf("%s ", name); \
                       printf(ANSI_COLOR_GREEN "TEST PASSED\n" ANSI_COLOR_RESET );\
                   }}

/**
 * @brief Structure containing initialization parameters for GA-based optimization. 
 *
 * A structure that contains all the parameters for specifying a Genetic Algorithms 
 * simulation, including how many generations it will run for, what the size of
 * the population is, what the size of the genome is, what the endpoints of the
 * interval to optimize on are, how many offspring are to be produced and how many
 * individuals are to be replaced.
 * It is typical in a Genetic Algorithm without elitism to replace the 
 * entire population. The number of indepedent runs can also be specified, if you are 
 * trying to derive an Monte Carlo estimate of fitness.
 */
typedef struct parameter_ga {
    REAL_ a;    /**< Lower bound of genome's interval ([a, b]) */
    REAL_ b;    /**< Upper bound of genome's interval ([a, b]) */
    std::size_t generations;    /**< Number of generations the GA will run for */
    std::size_t population_size;    /**< Population size (number of individuals) */
    std::size_t genome_size;    /**< Size of genome of each individual */
    std::size_t num_offsprings; /**< Number of offsprings (children) */
    std::size_t num_replacement;    /**< Number of individuals being replaced in every generation */
    int runs;   /**< Number of experimental runs, greater than 1 in case of multiple trajectories */
} ga_parameter_s;


/**
 * @brief Structure containing initialization parameters for an Island Model.
 *
 * A structure that contains all the parameters for running an Island Model
 * (IM), a GA with multiple subpopulations and structured migrations.
 */
typedef struct parameter_im {
    std::size_t num_immigrants; /**< Number of immigrants (individuals to move) */
    std::size_t num_islands;    /**< Number of islands (threads) in the model */
    std::size_t migration_interval; /**< Migration interval (period) */
    std::string pick_method;    /**< Name of method for selecting migrants */
    std::string replace_method; /**< Name of method for displacing residents */
    std::string adj_list_fname; /**< Name of the file containing adjacency list (pop. graph) */
    bool is_im_enabled = false; /**< Boolean flag indicating if IM is enabled or not */
} im_parameter_s;


/**
 * @brief Structure containing display parameters controlling which information
 * is printed.
 *
 * A structure that contains all the parameters for printing information about
 * the GA or the IM. 
 */
typedef struct parameter_pr {
    std::string where2write;    /**< Path where results files should be written. */
    std::string experiment_name;    /**< Name of the experiment */
    bool print_fitness;     /**< Boolean flag for printing individual fitnesses */
    bool print_average_fitness; /**< Boolean flag for printing average fitness of a population */
    bool print_bsf; /**< Boolean flag for printing best-so-far fitness of a population */
    bool print_best_genome; /**< Boolean flag for printing the best genome of a population */
} pr_parameter_s;


/**
 * @brief Structure representing an individual.
 *
 * A structure that represents an individual and carries all the necessary 
 * information about an individual, including ID, fitness, and genome.
 */
typedef struct individual {
    std::size_t id;     /**< Individual's Unique ID */
    REAL_ fitness;      /**< Individual's Fitness value */
    std::vector<REAL_> genome;  /**< Individual's Genome (vector of REAL_)*/
} individual_s;


#ifdef __cplusplus
/**
 * @brief Genetic Algorithm main class. 
 *
 * GA is the main class for seting up and running an optimization process. It
 * contains all the methods for performing operations such as selection, 
 * crossover, and mutation.
 */
class GA {
    public:
        GA(ga_parameter_s *);  /**< Constructor method of GA class */
        ~GA();              /**< Destructor method of GA */

        /**
         * Genetic Algorithm basic operators
         */
        /// Selection operator method
        std::pair<individual_s, individual_s> selection(int);
        /// Crossover operator method
        std::vector<REAL_> crossover(std::vector<REAL_>, std::vector<REAL_>);
        /// Mutation operator method
        std::vector<REAL_> mutation(std::vector<REAL_>, REAL_, REAL_);

        /**
         * Genetic Algorithm secondary operations
         */
        /// Evaluation of fitness of individuals
        void evaluation(std::vector<individual_s>&);
        /// Generate the next generation out of current individuals and
        // their offsprings
        void next_generation(std::size_t);
        /// Sorting individuals based on their fitness value
        void sort_population(void);
        /// Main routine for running one single generation step
        void run_one_generation(std::size_t iteration=0);
        /// Main routine for evolving a population over generations
        void evolve(std::size_t, std::size_t, pr_parameter_s *); 

        /// Fitness function - function pointer
        REAL_ (*fitness)(std::vector<REAL_>&);

        std::vector<individual_s> population; /// Individuals population vector
        std::vector<individual_s> offsprings;   /// Offsprings vector 
        std::vector<individual_s> sorted_population;  /// Sorted population vector
        individual_s best_individual;   /// Best individual (keep track)
        std::vector<REAL_> bsf;     /// BSF vector (keep track)
        std::vector<REAL_> fit_avg;  /// Average fitness vector (keep track)
        std::vector<REAL_> hfi; /// Highest fitness in the population
        std::vector<REAL_> lfi; /// Lowest fitness in the population

    private:
        REAL_ alpha, beta;  /// Genome's interval limits [a, b]
        std::vector<individual_s> immigrant;    /// Immigrants vector (buffer)
        REAL_ best_so_far;  /// Best so far (BSF) vector
        std::size_t mu;     /// Number of individuals within a population
        std::size_t lambda; /// Number of offsprings
        std::size_t replace_perc;   /// Number of individuals being replaced 
        std::size_t genome_size;    /// Genome size (number of genes)

    friend class IM;
};


/**
 * @brief Island Model main class.
 * 
 * Island Model class contains methods to run an Island Model (IM)
 * optimization. The class instance reads the topology from a file and builds 
 * a map (adjacency list). Moreover, provides methods for individuals selection
 * and immigration over islands (threads). 
 */
class IM {
    public:
        IM(im_parameter_s *, ga_parameter_s *);    /// IM Constructor method
        ~IM();  /// IM Destructor method
        
        /**
         * Island Model basic operators methods
         */
        /// Selection method. It selects individuals per island to move and
        //populates the vacant spots with new ones randomly. 
        //@see select_ind2immigrate()
        void select_ind2migrate(std::size_t, std::size_t,
                                std::string method="random");
        /// Immigration method. It moves the immigrants (individuals) from one
        //island to another based on a specific policy.
        //@see move_immigrants()
        void move_immigrants(std::size_t, std::size_t, std::string);

        /**
         * Island Model secondary operations methods
         */
        /// Reads the connectivity graph (topology) from a file
        std::size_t read_connectivity_graph(std::string);
        /// Evolves an island (thread function)
        void evolve_island(std::size_t, im_parameter_s *, pr_parameter_s *);
        /// Runs the Island Models 
        void run_islands(im_parameter_s *, pr_parameter_s *);

        std::vector<GA> island; /// Islands (threads) vector

    private:
        REAL_ a, b; /// Genome's interval [a, b]
        std::map<int, std::vector<int>> adj_list;   /// Adjacent list of islands
        size_t generations;     /// Generations
        size_t num_immigrants;  /// Number of immigrants
        size_t num_islands;     /// Number of islands (threads)
        size_t migration_interval;  /// Migration interval
        std::size_t migration_steps;    /// Generations / Migration Interval 

        std::mutex mtx;     // Mutex for locking threads
};

#endif  /* __cplusplus  */

#ifdef __cplusplus
extern "C" {
#endif
/// Extra Selection functions
std::pair<individual_s, individual_s> roulette_wheel_selection(std::vector<individual_s> &);
individual_s sus_selection();
std::pair<individual_s, individual_s> rank_selection(std::vector<individual_s> &);
std::pair<individual_s, individual_s> random_selection(std::vector<individual_s> &);


/// Extra Crossover functions
std::vector<REAL_> two_point_crossover(std::vector<REAL_>, std::vector<REAL_>);
std::vector<REAL_> uniform_crossover(std::vector<REAL_>, std::vector<REAL_>);
std::vector<REAL_> flat_crossover(std::vector<REAL_>, std::vector<REAL_>);
std::vector<REAL_> discrete_crossover(std::vector<REAL_>, std::vector<REAL_>);
std::vector<REAL_> order_one_crossover(std::vector<REAL_>, std::vector<REAL_>);


/// Extra Mutation functions
std::vector<REAL_> random_mutation(std::vector<REAL_>, REAL_, REAL_);
std::vector<REAL_> nonuniform_mutation(std::vector<REAL_>, std::size_t,
                                       std::size_t, std::size_t,
                                       REAL_, REAL_);
std::vector<REAL_> fusion_mutation(std::vector<REAL_>);
std::vector<REAL_> swap_mutation(std::vector<REAL_>);


/// Auxiliary Functions declarations
bool compare_fitness(const individual_s, const individual_s);
REAL_ average_fitness(REAL_, const individual_s);
bool is_path_exist(const std::string &);
int remove_file(const std::string);


/// Multithread and Islands functions declarations
void independent_runs(ga_parameter_s *, pr_parameter_s *);


/// Printing Functions
std::string center(const std::string, const int);
std::string prd(const double, const int, const int);
void print_fitness(std::vector<individual_s> &, std::size_t unique_id=0,
                   std::string write_to="stdout");
void print_bsf(std::vector<REAL_>, std::size_t unique_id=0,
               std::string write_to="stdout");
void print_avg_fitness(std::vector<REAL_>, std::size_t unique_id=0,
                       std::string write_to="stdout");
void print_best_genome(std::vector<REAL_>, std::size_t unique_id=0,
                       std::string write_to="stdout");



/// Demo functions
/// End-user can define more at her/his will

REAL_ sphere(std::vector<REAL_>&);
REAL_ rastrigin(std::vector<REAL_>&);
REAL_ schwefel(std::vector<REAL_>&);
REAL_ griewangk(std::vector<REAL_>&);
REAL_ tsm(std::vector<REAL_>& x);


/// Parameters files
std::tuple<ga_parameter_s, pr_parameter_s, im_parameter_s> read_parameters_file(const char *);
void print_parameters(ga_parameter_s, pr_parameter_s, im_parameter_s); 
bool is_path_exist(const std::string &);
int mkdir_(const std::string &);

#ifdef __cplusplus
}
#endif
#endif  // GENETIC_ALG_H
