import numpy as np
import matplotlib.pylab as plt


if __name__ == '__main__':
    a = np.array([1.28, 5.42, 9.79, 18.3])
    b = np.array([0.028, 0.062, 0.114, 0.194])
    c = np.array([0.34, 0.39, 0.43, 0.57])
    x = np.array([2, 16, 32, 64])

    im12p = np.array([2.876, 11.934, 24.461])
    im8p = np.array([2.098, 8.502, 18.001])
    im4p = np.array([1.258, 5.372, 13.083])
    im2p = np.array([1.006, 4.344, 10.449])

    im12mp = np.array([4.64, 20.961, 40.869])
    im8mp = np.array([4.015, 20.222, 37.056])
    im4mp = np.array([2.411, 9.711, 20.960])
    im2mp = np.array([1.961, 7.857, 17.517])

    indp12 = np.array([2.818, 11.839, 25.052])
    indp8 = np.array([2.065, 8.472, 18.015])
    indp4 = np.array([1.246, 5.334, 11.073])
    indp2 = np.array([1.011, 4.339, 8.986])

    indmp12 = np.array([3.843, 18.572, 37.843])
    indmp8 = np.array([3.333, 13.603, 29.062])
    indmp4 = np.array([2.215, 9.148, 18.996])
    indmp2 = np.array([1.896, 7.525, 16.150])

    indmpi = np.array([16.150, 18.996, 29.062, 37.585])
    indpos = np.array([8.986, 11.073, 18.015, 25.052])

    fig = plt.figure(figsize=(13, 7))
    fig.subplots_adjust(bottom=.2, left=.14)
    fig.subplots_adjust(wspace=.25, hspace=.5, bottom=.2, right=.85)
    ax = fig.add_subplot(221)
    # ax.semilogy(x, a, c='orange', marker='x', ms=5)
    # ax.semilogy(x, b, c='k', marker='o', ms=5)
    ax.semilogy(x, a, c='orange', alpha=0.7, marker='x', ms=5, label='PyGAIM')
    ax.semilogy(x, b, c='k', alpha=0.7, marker='o', ms=5, label='GAIM')
    ax.semilogy(x, c, c='g', alpha=0.7, marker='o', ms=5, label='ECJ')
    ax.set_xlabel('Genome Size', fontsize=16, weight='bold')
    ax.set_ylabel('Performance (secs)', fontsize=16, weight='bold')
    ax.set_ylim([10**-2, 10**2])
    ticks = ax.get_yticks()
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.set_xticks([2, 16, 32, 64])
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    # ax.spines['right'].set_visible(False)
    # ax.spines['top'].set_visible(False)
    # ax.yaxis.set_ticks_position('left')
    # ax.xaxis.set_ticks_position('bottom')
    ax.text(2, 260, 'A',
            va='top',
            ha='left',
            fontsize=16,
            weight='bold')

    ax.legend(loc=2)

    ax = fig.add_subplot(222)
    x = np.array([2, 4, 8, 12])
    ax.semilogy(x, indpos, c='k', alpha=0.7, marker='x', ms=5,
                label='POSIX')
    ax.semilogy(x, indmpi, c='b', alpha=0.7, marker='x', ms=5,
                label='OpenMPI')
    ax.set_ylim([10**-1, 10**2])
    ax.set_xlabel('Number of Threads/Tasks', fontsize=16, weight='bold')
    # ax.set_yticks([])
    ax.set_xticks([2, 4, 8, 12])
    ticks = ax.get_xticks().astype('i')
    # ax.axes.get_yaxis().set_visible(False)
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = ax.get_yticks()
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    # ax.spines['right'].set_visible(False)
    # ax.spines['top'].set_visible(False)
    # ax.yaxis.set_ticks_position('left')
    # ax.xaxis.set_ticks_position('bottom')
    ax.text(2, 260, 'B',
            va='top',
            ha='left',
            fontsize=16,
            weight='bold')
    ax.legend()

    ax = fig.add_subplot(223)
    x = np.array([2, 4, 8, 12])
    yp = [im2p[-1], im4p[-1], im8p[-1], im12p[-1]]
    ymp = [im2mp[-1], im4mp[-1], im8mp[-1], im12mp[-1]]
    ax.semilogy(x, yp, c='k', alpha=0.7, marker='x', ms=5, label='POSIX')
    ax.semilogy(x, ymp, c='b', alpha=0.7, marker='o', ms=5, label='OpenMPI')
    ax.set_ylim([10**-1, 10**2])
    ax.set_xlabel('Number of Threads/Tasks', fontsize=16, weight='bold')
    ax.set_ylabel('Performance (secs)', fontsize=16, weight='bold')
    # ax.set_yticks([])
    ax.set_xticks([2, 4, 8, 12])
    ticks = ax.get_xticks().astype('i')
    # ax.axes.get_yaxis().set_visible(False)
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = ax.get_yticks()
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    # ax.spines['right'].set_visible(False)
    # ax.spines['top'].set_visible(False)
    # ax.yaxis.set_ticks_position('left')
    # ax.xaxis.set_ticks_position('bottom')
    ax.text(2, 260, 'C',
            va='top',
            ha='left',
            fontsize=16,
            weight='bold')
    ax.legend()

    ax = fig.add_subplot(224)
    x = np.array([10, 50, 100])
    mrks = ['x', 'o', '.']
    ax.semilogy(x, im2p, c='k', alpha=1, marker='x', ms=5, label='2 POSIX')
    ax.semilogy(x, im4p, c='b', alpha=1, marker='o', ms=5, label='4 POSIX')
    ax.semilogy(x, im8p, c='orange', alpha=1, marker='.', ms=5, label='8 POSIX')
    ax.semilogy(x, im12p, c='m', alpha=1, marker='p', ms=5, label='12 POSIX')
    ax.semilogy(x, im2mp, c='g', alpha=0.5, marker='2', ms=5,
                label='2 OpenMPI')
    ax.semilogy(x, im4mp, c='y', alpha=0.5, marker='^', ms=5,
                label='4 OpenMPI')
    ax.semilogy(x, im8mp, c='orange', alpha=0.5, marker='v', ms=5,
                label='8 OpenMPI')
    ax.semilogy(x, im12mp, c='c', alpha=0.5, marker='h', ms=5,
                label='12 OpenMPI')
    # ax.spines['right'].set_visible(False)
    # ax.spines['top'].set_visible(False)
    # ax.yaxis.set_ticks_position('left')
    # ax.xaxis.set_ticks_position('bottom')
    # ax.set_yticks([])
    # ax.axes.get_yaxis().set_visible(False)
    ax.set_ylim([10**-1, 10**2])
    ax.set_xlabel('Population Size', fontsize=16, weight='bold')
    ax.set_xticks([10, 50, 100])
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = ax.get_yticks()
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')
    ax.legend(bbox_to_anchor=(1.05, 1.))
    ax.text(10, 260, 'D',
            va='top',
            ha='left',
            fontsize=16,
            weight='bold')

    plt.savefig('performance.png', axis='tight')
    plt.show()
