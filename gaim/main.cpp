#include "gaim.h"

int main() {
    std::tuple<ga_parameter_s, pr_parameter_s, im_parameter_s> pms;
    ga_parameter_s ga_pms;
    pr_parameter_s pr_pms;
    im_parameter_s im_pms;

    std::tie(ga_pms, pr_pms, im_pms) = read_parameters_file("./examples/demo.cfg");
    print_parameters(ga_pms, pr_pms, im_pms);

    if (im_pms.is_im_enabled) {
        std::cout << "Running an Island Model" << std::endl;
        IM island_model(&im_pms, &ga_pms);
        island_model.run_islands(&im_pms, &pr_pms);
    } else {
        if (ga_pms.runs == 1) {
            std::cout << "Running a single GA" << std::endl;
            GA gen_alg(&ga_pms);
            // gen_alg.fitness = &tsm;
            gen_alg.fitness = &schwefel;
            gen_alg.evolve(ga_pms.generations, 0, &pr_pms);
        } else if (ga_pms.runs > 1) {
            std::cout << "Running "<< ga_pms.runs << " Independent GAs" << std::endl;
            independent_runs(&ga_pms, &pr_pms);
        } else {
            std::cout << "Negative number of runs is illegal!" << std::endl;
            exit(-1);
        }
    }
    return 0;
}
