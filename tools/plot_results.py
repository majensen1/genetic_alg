import os
import sys
import glob
import numpy as np
from struct import unpack
import matplotlib.pylab as plt


def plot_data(directory='./data/', data_type="bsf"):
    if os.path.isdir(directory) is not True:
        print("Directory does not exist!")
        sys.exit(-1)

    fnames = glob.glob(directory+'/*.dat')

    i = 0
    data = []
    for name in fnames:
        if data_type in name:
            with open(name, 'rb') as f:
                c = f.read()
            data.append(np.array(unpack('f'*(len(c)//4), c), 'f'))

    fig = plt.figure(figsize=(9, 5))
    fig.subplots_adjust(bottom=0.2, left=0.2)
    ax = fig.add_subplot(111)
    for i in range(len(data)):
        ax.plot(data[i], lw=2)
    ax.set_xlabel("Generations", fontsize=16, weight='bold')
    ax.set_ylabel(data_type, fontsize=16, weight='bold')
    ticks = ax.get_xticks().astype('i')
    ax.set_xticklabels(ticks, fontsize=16, weight='bold')
    ticks = np.round(ax.get_yticks(), 4)
    ax.set_yticklabels(ticks, fontsize=16, weight='bold')


def print_fitness(directory='./data/', islands=3, pop_size=[20, 20, 20]):
    for i in range(islands):
        fname = directory+"fitness_"+str(i)+".dat"
        f = open(fname, 'rb')
        print("Island # %d" % (i))
        for _ in range(pop_size[i]):
            c, d = f.read(4), f.read(4)
            idx, fit = unpack('i', c), unpack('f', d)
            print(idx[0], fit[0])
        print("---------------------------------------")


def print_best_genome(directory='./data/', islands=1):
    data = []
    for i in range(islands):
        fname = directory+"best_genome_"+str(i)+".dat"
        with open(fname, 'rb') as f:
            c = f.read()
        size = len(c) // 4
        dec = unpack('f'*size, c)
        data.append(dec)

    for i, d in enumerate(data):
        print("Best Genome of Island # %d" % (i))
        print(d)


if __name__ == '__main__':
    plot_data(data_type="bsf")      # Plot BSF
    plot_data(data_type="average_fitness")      # Plot Average Fitness
    # print_fitness()      # Print individuals IDs and their fitness
    print_best_genome()
    plt.show()
