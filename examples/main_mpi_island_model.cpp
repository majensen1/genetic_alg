/* GAIM MPI Island Model -- C++ example file 
 * Copyright (C) 2019  Georgios Detorakis (gdetor@protonmail.com)
 *                     Andrew Burton (ajburton@uci.edu)
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. */

// $Id$
/**
 * @file main_mpi_island_model.cpp
 * How to use MPI to run Island Model if it's required (e.g. on a
 * cluster).
 */
// $Log$
#include "gaim.h"
#include "mpi.h"


/**
 * @brief Structure containing information about MPI's communication graph for
 * the Island Model implementation.
 *
 * A structure that contains all the parameters for each node on the
 * Communication Graph of MPI.
 */
typedef struct node_struct {
    int ideg;        /**< Input degree of the node. */
    int odeg;        /**< Output degree of the node. */
    int *dsts;       /**< Destinations of the node. */
} node_s;


/**
 * @brief Reads the connectivity graph from file and builds the corresponding
 * communication graph used by MPI. 
 *
 * It initializes all the necessary parameters and vectors for evolving a 
 * population of individuals based on a predetermined Genetic Algorithm (GA). 
 *
 * @param[in] fname         Name of the file contains the adjacent graph
 * @param[in] num_islands   Number of islands (MPI processes)
 * @return A C++ map that maps the number of an MPI process with the
 * destination processes.
 */
// !FIXME There is a potential memory leak issue in this function
std::map<int, node_s> read_connectivity_graph(std::string fname,
                                              std::size_t num_islands)
{
    int source, content;
    std::size_t num_vertices, num_edges;
    node_s tmp;
    std::map<int, node_s> adj_list;   // Adjacent list of islands

    auto ifile = std::fstream(fname, std::ios::in);
    if (!ifile) {
        std::cout << "Unable to open file " << fname << std::endl;
        exit(1);
    }

    ifile >> num_vertices;
    for (size_t i = 0; i < num_vertices; ++i) {
        ifile >> source;
        ifile >> num_edges;
        tmp.odeg = num_edges;
        tmp.ideg = num_edges;
        tmp.dsts = (int *)malloc(sizeof(int) * tmp.odeg);
        for (size_t j = 0; j < num_edges; ++j) {
            ifile >> content;
            if (content < 0 && content > (int) num_islands) {
                std::cerr << "Error: Invalid destination island!" << std::endl;
                exit(-1);
            }
            tmp.dsts[j] = content;
        }
        adj_list.emplace(std::make_pair(source, tmp));
        // free(tmp.dsts);
    }
    ifile.close();
    return adj_list;
}


/**
 * @brief Chooses the immigrants to from each island (MPI process)
 *
 * It takes as argument the buffer that will be populated with individuals
 * as potential migrants.
 * It supports three methods of distribution. random - where individuals are
 * randomly chosen, poor - individuals with the lowest fitness are chosen, 
 * and finally elit - where individuals with the best fitness are chosen.
 *
 * @param[in] pool Is the vector of indviduals within an Island (MPI process) 
 * @param[in] buffer A buffer array that is populated with potential migrants
 * @param[in] (*f) A pointer to a function (fitness)
 * @param[in] a (float) The lower bound of fitness function domain
 * @param[in] b (float) The upper bound of fitness function domain
 * @param[in] num_immigrants Number of individuals to be chosen
 * @param[in] genome_size Size of individuals genome
 * @param[in] method   A string that describes the method of replacement
 * @return Nothing
 */
void select_ind2migrate(std::vector<individual_s> pool,
                        REAL_ **buffer,
                        REAL_ (*f)(std::vector<REAL_> &),
                        REAL_ a,
                        REAL_ b,
                        std::size_t num_immigrants,
                        std::size_t genome_size,
                        std::string method)
{
    std::size_t id;
    std::size_t len = pool.size();
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_real_distribution<> probs(a, b);
    std::vector <REAL_> new_genome(genome_size);
    static std::vector<int> pop(pool.size());

    std::iota(std::begin(pop), std::end(pop), 0);
    std::shuffle(std::begin(pop), std::end(pop), gen);

    if (method == "random") {
        for (size_t i = 0; i < num_immigrants; ++i) {
            id = pop[i];
            std::copy(pool[id].genome.begin(),
                      pool[id].genome.end(), 
                      *buffer + (i*genome_size));
            std::generate(new_genome.begin(),
                          new_genome.end(),
                          [&]{return probs(gen);});
            pool[id].genome = new_genome;
            pool[id].fitness = f(new_genome);
        }
    } else if (method == "elit") {
        for (size_t i = len-1, j = 0; j < num_immigrants; --i, ++j) {
            std::copy(pool[i].genome.begin(),
                      pool[i].genome.end(),
                      *buffer + (j*genome_size));
            std::generate(new_genome.begin(),
                          new_genome.end(),
                          [&]{return probs(gen);});
            pool[i].genome = new_genome;
            pool[i].fitness = f(new_genome);
        }
    } else if (method == "poor") {
        for (size_t i = 0; i < num_immigrants; ++i) {
            std::copy(pool[i].genome.begin(),
                      pool[i].genome.end(),
                      *buffer + (i*genome_size));
            std::generate(new_genome.begin(),
                          new_genome.end(),
                          [&]{return probs(gen);});
            pool[i].genome = new_genome;
            pool[i].fitness = f(new_genome);
        }
    } else {
        std::cerr << "No such immigration method exists!" <<std::endl;
        exit(-1);
    }
}


/**
 * @brief Distributes the immigrants to their destination MPI processes. 
 *
 * It takes as argument the buffer containing all the migrants and distributes
 * them to their final destination MPI process. It supports three methods of 
 * distribution. random - where random individuals from within the local
 * pooulation are being replaced, poor - where individuals with the lowest
 * fitness are replaced, and finally elit - where individuals with the best 
 * fitness are replaced by migrants.
 *
 * @param[in] pool Is the vector of indviduals within an Island (MPI process) 
 * @param[in] buffer A buffer array that contains the migrants
 * @param[in] num_immigrants Number of individuals to be replaced
 * @param[in] genome_size Size of individuals genome
 * @param[in] method   A string that describes the method of replacement
 * @return Nothing
 */
void receiving_immigrants(std::vector<individual_s> pool,
                          REAL_ *buffer,
                          REAL_ (*f)(std::vector<REAL_> &),
                          std::size_t num_immigrants,
                          std::size_t genome_size,
                          std::string method)
{
    std::size_t id;
    static std::random_device rd;
    static std::mt19937 gen(rd());
    std::vector<individual_s> tmp(num_immigrants);
    static std::vector<int> pop(pool.size());

    std::iota(std::begin(pop), std::end(pop), 0);
    std::shuffle(std::begin(pop), std::end(pop), gen);

    if (method == "random") {
        for (size_t i = 0; i < num_immigrants; ++i) {
            id = pop[i];
            std::copy(buffer+(i*genome_size),
                      buffer+((i+1)*genome_size),
                      pool[id].genome.begin());
            pool[id].fitness = f(pool[id].genome);
        }
    } else if (method == "poor") {
        for (size_t i = 0; i < num_immigrants; ++i) {
            std::copy(buffer+(i*genome_size),
                      buffer+((i+1)*genome_size),
                      pool[i].genome.begin());
            pool[i].fitness = f(pool[i].genome);
        }
    } else if (method == "elit") {
        id = pool.size();
        for (size_t i = 0; i < num_immigrants; ++i) {
            std::copy(buffer+(i*genome_size),
                      buffer+((i+1)*genome_size),
                      pool[id-1-i].genome.begin());
            pool[id-1-i].fitness = f(pool[id-1-i].genome);
        }
    } else {
        std::cerr << "No such immigration method exists!" <<std::endl;
        exit(-1);
    }
}


int main(int argc, char **argv)
{
    int rank, size, flag;
    int ideg, odeg, wgt;
    int *in = NULL, *out = NULL;
    int *dsts = NULL;
    size_t data_size;

    std::map<int, node_s> adj_list;
    std::tuple<ga_parameter_s, pr_parameter_s, im_parameter_s> pms;
    ga_parameter_s ga_pms;
    pr_parameter_s pr_pms;
    im_parameter_s im_pms;

    std::tie(ga_pms, pr_pms, im_pms) = read_parameters_file("demo_island_model.cfg");
    print_parameters(ga_pms, pr_pms, im_pms);

    // Number of data get transfered
    data_size = im_pms.num_immigrants*ga_pms.genome_size;
    
    // Compute the migration steps
    size_t migration_steps = ga_pms.generations / im_pms.migration_interval;
    
    // Initialize MPI
    MPI_Init(&argc, &argv);
    // Get the rank of the current process
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    // Get the size of the world
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if (size != im_pms.num_islands) {
        std::cerr << "Number of Islands mistmatches MPI rank size!" << std::endl;
        exit(-1);
    }

    MPI_Comm comm_dist_graph;
    
    // Read the connectivity graph from a file
    adj_list = read_connectivity_graph(im_pms.adj_list_fname,
                                       im_pms.num_islands);
    
    REAL_ **sbuf = (REAL_ **)malloc(sizeof(REAL_ *) * im_pms.num_islands);
    REAL_ **rbuf = (REAL_ **)malloc(sizeof(REAL_ *) * im_pms.num_islands);
    for (size_t i = 0; i < im_pms.num_islands; ++i) {
        sbuf[i] = (REAL_ *)calloc(data_size, sizeof(REAL_));
        rbuf[i] = (REAL_ *)calloc(data_size, sizeof(REAL_));
    }

    // Define islands topology and determine the MPI graph
    flag = MPI_Dist_graph_create_adjacent(MPI_COMM_WORLD, adj_list[rank].odeg, 
                                          adj_list[rank].dsts, MPI_UNWEIGHTED,
                                          adj_list[rank].odeg, adj_list[rank].dsts, 
                                          MPI_UNWEIGHTED, MPI_INFO_NULL, 1,
                                          &comm_dist_graph);

    // Get neighbors count 
    MPI_Dist_graph_neighbors_count(comm_dist_graph, &ideg, &odeg, &wgt);

    in = (int *)malloc(ideg * sizeof(int));
    out = (int *)malloc(odeg * sizeof(int));

    // Get neighbors dist
    MPI_Dist_graph_neighbors(comm_dist_graph, ideg, in, MPI_UNWEIGHTED,
                             odeg, out, MPI_UNWEIGHTED);

    // Instantiate the GA class
    GA gen_alg(&ga_pms);

    // Define the fitness function
    gen_alg.fitness = &sphere;

    // Evaluate individuals 
    gen_alg.evaluation(gen_alg.population);

    MPI_Request *rreqs, *sreqs;
    rreqs = (MPI_Request *)malloc(sizeof(MPI_Request) * ideg);
    sreqs = (MPI_Request *)malloc(sizeof(MPI_Request) * odeg);
    for (size_t i = 0; i < ga_pms.generations; ++i) {
        // Run an evolution step on each island
        gen_alg.run_one_generation(i);
        
        // Migration takes place
        if (i % im_pms.migration_interval == 0) {
            // Pick up the candidate immigrants
            /* select_ind2migrate(gen_alg.sorted_population, */
            /*                    &sbuf[rank], */
            /*                    gen_alg.fitness, */
            /*                    ga_pms.a, */
            /*                    ga_pms.b, */
            /*                    im_pms.num_immigrants, */
            /*                    ga_pms.genome_size, */
            /*                    im_pms.pick_method); */
            // printf("Rank: %d  Selection done\n", rank);  
            // Receive immigrants
            for (int j = 0; j < ideg; ++j) {
                MPI_Irecv(rbuf[rank], data_size, MPI_FLOAT, in[j], 99,
                          comm_dist_graph, &rreqs[j]);
            }

            // Send immigrants
            for (int j = 0; j < odeg; ++j) {
                MPI_Isend(&sbuf[rank], data_size, MPI_FLOAT, out[j], 99,
                          comm_dist_graph, &sreqs[j]);
            }
    
            // Wait for all the process to move information
            MPI_Waitall(ideg, rreqs, MPI_STATUSES_IGNORE);
            MPI_Waitall(odeg, sreqs, MPI_STATUSES_IGNORE);

        /*     // Move immigrants */
        /*     receiving_immigrants(gen_alg.sorted_population, */
        /*                          rbuf[rank], */
        /*                          gen_alg.fitness, */
        /*                          im_pms.num_immigrants, */
        /*                          ga_pms.genome_size, */
        /*                          im_pms.replace_method); */
        }
        MPI_Barrier(comm_dist_graph);
    }
    
    for (size_t i = 0; i < 0; ++i) {
        free(rbuf[i]);
        free(sbuf[i]);
    }
    free(rbuf);
    free(sbuf);
    free(in);
    free(out);
    free(dsts);
    free(rreqs);
    free(sreqs);
    
    // Finalize MPI
    MPI_Finalize();
}
