/* Genetic Algorithm Class cpp file for GAIM software 
 * Copyright (C) 2019  Georgios Detorakis (gdetor@protonmail.com)
 *                     Andrew Burton (ajburton@uci.edu)
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. */

// $Id$
/**
 * @file genetic_alg.cpp
 * Defines the Genetic Algorithm class which controls evolutionary optimization.
 */
// $Log$
#include "gaim.h"
#include <numeric>


/**
 * @brief Constructor of GA class. 
 *
 * It initializes all the necessary parameters and vectors for evolving a 
 * population of individuals based on a predetermined Genetic Algorithm (GA). 
 *
 * @param[in] ga_pms    A structure that contains all the parameters for the GA
 * @return Nothing
 */
GA::GA(ga_parameter_s *ga_pms)
{
    alpha = ga_pms->a;  // Lower bound for genes [a, b]
    beta = ga_pms->b;   // Upper bound for genes [a, b]

    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> genes(alpha, beta);

    mu = ga_pms->population_size;   // Population size
    lambda = ga_pms->num_offsprings;    // Number of offspring
    replace_perc = ga_pms->num_replacement; // Number of individuals being replaced
    if (replace_perc > lambda) {
        std::cout << "Generation replacement dimension is larger than \
            offsprings population size!" << std::endl;
        exit(-1);
    }
    genome_size = ga_pms->genome_size;  // Genome size
    best_so_far = -10000;               // Best-so-far fitness

    // Initialize the population vector
    for (std::size_t i = 0; i < mu; ++i) {
        population.push_back(individual_s());
        population[i].id = i;               // Unique ID
        population[i].fitness = -10000;     // Fitness value
        for (std::size_t j = 0; j < genome_size; ++j) {
            population[i].genome.push_back(genes(gen)); // Genome
        }
    }
    sorted_population = population; // Sort the population based on fitness

    fitness = &sphere;  // Define the cost function (example -> sphere)

    // Initialize the offsprings vector
    for (std::size_t i = 0; i < lambda; ++i) {
        offsprings.push_back(individual_s());
        offsprings[i].id = i;
        offsprings[i].fitness = -10000;
        for (std::size_t j = 0; j < genome_size; ++j) {
            offsprings[i].genome.push_back(genes(gen)); 
        }
    }
}


/**
 * @brief Destructor of GA class
 */
GA::~GA()
{
    population.clear();
    offsprings.clear();
    sorted_population.clear();
}


/**
 * Evaluates the fitness of each individual based on a predefined cost
 * function. 
 *
 * @param[in] x Vector of individuals (population)
 * @return Nothing (void)
 *
 * @see test_functions.cpp
 */
void GA::evaluation(std::vector<individual_s> &x)
{
    for (std::size_t i = 0; i < x.size(); ++i) {
        x[i].fitness = fitness(x[i].genome);
    }
}


/**
 * Sorts the individuals within a population based on their fitness value.
 *
 * @param[in] void
 * @return Nothing (void)
 */
void GA::sort_population(void)
{
    sorted_population = population;
    std::sort(sorted_population.begin(),
              sorted_population.end(),
              compare_fitness);
}


/**
 * Applies a selection operator by selecting two parents for breeding out
 * of a whole population of individuals. By default this method implements a
 * k-tournament selection. 
 *
 * @param[in] k An integer that defines the tournament size (how many 
 *              individuals from the population will be drawn randomly)
 * @return A pair of individuals (parent1 and parent2)
 *
 * @note The user can define his or her own selection operators or can use any
 * other operator defined in the file selection.cpp.
 *
 * @see roulette_wheel_selection(), rank_selection(), random_selection()
 */
std::pair<individual_s, individual_s> GA::selection(int k=2)
{
    individual_s ind, parent1, parent2, best, better;
    std::vector<int> pop(population.size());
    static std::random_device rd;
    static std::mt19937 gen(rd());

    std::iota(std::begin(pop), std::end(pop), 0);
    std::shuffle(std::begin(pop), std::end(pop), gen);

    for (int i = 0; i < k; ++i) {
        ind = population[pop[i]];
        if ((parent1.genome.size() == 0) ||
            (fitness(ind.genome) > fitness(parent1.genome))) {
            parent1 = ind;
        }
    }

    pop.erase(pop.begin(), pop.begin()+k);

    for (int i = 0; i < k; ++i) {
        ind = population[pop[i]];
        if ((parent2.genome.size() == 0) ||
            (fitness(ind.genome) > fitness(parent2.genome))) {
            parent2 = ind;
        }
    }
    return std::make_pair(parent1, parent2);
}


/**
 * Applies a crossover operator on the genomes of two parents (parent1 and
 * parent2) to create a child genome.
 * By default, this is a one-point crossover operator.
 *
 * @param[in] parent1 Genome of the first parent
 * @param[in] parent2 Genome of the second parent
 * @return The genome of a child as a vector of type REAL_
 *  
 * @note The user can define its his or her own crossover operator or they can 
 * use any mutation operator that is defined in the file crossover.cpp.
 *
 * @see two_point_crossover(), uniform_crossover(), flat_crossover(),
 * discrete_crossover()
 */
std::vector<REAL_> GA::crossover(std::vector<REAL_> parent1,
                                 std::vector<REAL_> parent2)
{
    int order;
    std::size_t xover;
    std::vector<REAL_> child1, child2;
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> uniform_num(0, parent1.size()-1);
    static std::uniform_int_distribution<> choice(0, 1);

    xover = uniform_num(gen);
    order = choice(gen);
    
    child1 = parent1;
    child2 = parent2;
    for (std::size_t i = xover; i < child1.size(); ++i) {
        child1[i] = parent2[i];
        child2[i] = parent1[i];
    } 
    
    if (order == 0) {
        return child1;
    } else {
        return child2;
    }
}


/**
 * The basic mutation operator. By default, this is a delta mutation operator 
 * meaning that for every gene in the genome a random number drawn from a normal
 * distribution is added to the gene value according to a probability. Specifically,
 * the mutation delta is drawn from the Normal/Gaussian distribution with mean 0 and 
 * standard deviation 0.5.
 *
 * @param[in] genome The genome (genes) of an individual
 * @param[in] mutation_rate Mutation rate (probability threshold)
 * @param[in] variance  Standard deviation of the normal distribution from which the
 * increment/decrement is drawn
 * @return A vector of floats (mutated genome)
 *
 * @note The used can define his or her own method of mutation or they can use
 * any other operator defined in the file mutation.cpp.
 *
 * @see random_mutation(), nonuniform_mutation(), fusion_mutation()
 */
std::vector<REAL_> GA::mutation(std::vector<REAL_> genome,
                                REAL_ mutation_rate=0.5,
                                REAL_ variance=0.5)
{
    std::vector<REAL_> tmp;
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_real_distribution<> probs(0, 1);
    static std::normal_distribution<> delta(0, variance);

    tmp = genome;
    for (std::size_t i = 0; i < genome.size(); ++i) {
        if (probs(gen) <= mutation_rate) {
            tmp[i] += delta(gen);
        }
    }
    return tmp;
}


/**
 * The basic operation for forming the next generation. It determines which 
 * offspring individuals will pass their genomes to the next generation and 
 * which parents will be replaced. By default, it replaces the worst parents
 * by the best offspring. 
 *
 * @param[in] percentage The percentage of old individuals that will be
 *                       replaced by new individuals.
 * @return Nothing (void)
 *
 * @note The user can define his or her own method for the next generation.
 */
void GA::next_generation(std::size_t perc=3)
{
    std::sort(population.begin(), population.end(), compare_fitness);
    std::sort(offsprings.begin(), offsprings.end(), compare_fitness);
    if (perc > lambda) {
        std::cout << "Percentage of offspring is larger than the\
            available number of offspring!" << std::endl;
        exit(-1);
    }
    for (std::size_t i = 0, j = lambda-1; i < perc; ++i, --j) {
        population[i].genome = offsprings[j].genome;
        population[i].fitness = fitness(offsprings[j].genome);
    }
}


#ifndef CUSTOM
/**
 *  @brief One single step of evolution (this is the main GA algorithm).
 *
 *  Runs one step of evolution. This means that this method evaluates and 
 *  sorts a population of individuals. It then selects parents, crosses and
 *  mutates their genomes, and generates the proper amount of offspring.
 *  Then it replaces the old generation with a new one. 
 *  
 *  @param[in] iteration A number that indicates the current generation
 *  @return Nothing (void)
 */
void GA::run_one_generation(std::size_t iteration)
{
    /* int order; */
    /* std::size_t xover; */
    individual_s parent1, parent2;
    std::pair<individual_s, individual_s> parents;
    std::vector<REAL_> child;

    // Evaluate fitness of each individual
    evaluation(population);

    // Sort the entire population based on fitness
    sort_population();

    // Bookkeeping
    hfi.push_back(sorted_population[sorted_population.size()-1].fitness);   // Best fitness
    lfi.push_back(sorted_population[0].fitness);    // Lowest fitness

    // Average fitness across population
    REAL_ acc = std::accumulate(population.begin(),
                                population.end(),
                                0,
                                average_fitness);
    fit_avg.push_back(acc / static_cast<REAL_>(mu));
    
    best_so_far = std::max(best_so_far, hfi[iteration]);
    bsf.push_back(best_so_far);
    best_individual = sorted_population[sorted_population.size()-1];

    // Generate new offspring
    for(std::size_t i = 0; i < lambda; ++i) {
        // Parents selection
        parents = selection();
        parent1 = parents.first;
        parent2 = parents.second;
     
        // Crossover
         child = crossover(parent1.genome, parent2.genome);
        // child = order_one_crossover(parent1.genome, parent2.genome);

        // Mutation
         child = mutation(child);
        // child = swap_mutation(child);

        // Append the offspring genome list
        offsprings[i].genome = child;
    }
    // Evaluate offspring fitness
    evaluation(offsprings);
    
    // Integrate offspring in the initial population
    next_generation(replace_perc);
}
#endif

/**
 *  @brief Evolves a population based on previously defined operators.
 *
 *  Evolves a population of individuals based on operations such as selection,
 *  crossover, and mutation that have been predefined. It executes the  
 *  evolutionary step over the total number of generations. 
 *  Finally, it prints out results either to STDOUT or to file.
 *
 * @param[in] generations   Total number of generations
 * @param[in] unique_id     Unique ID of a thread in case Island Model or
 *                          Independent runs are used. Otherwise, can be any
 *                          number of type size_t
 * @param[in] pms           Structure of logging parameters
 * @return Nothing (void)   
 */
void GA::evolve(std::size_t generations,
                std::size_t unique_id,
                pr_parameter_s *pms)
{
    for(std::size_t i = 0; i < generations; ++i) {
        run_one_generation(i);
    }
    sort_population();

    if (pms->print_fitness) {
        print_fitness(population, unique_id, pms->where2write);
    }
    if (pms->print_average_fitness) {
        print_avg_fitness(fit_avg, unique_id, pms->where2write);
    }
    if (pms->print_bsf) {
        print_bsf(bsf, unique_id, pms->where2write);
    } 
    if (pms->print_best_genome) {
        print_best_genome(best_individual.genome, unique_id, pms->where2write);
    }
}
