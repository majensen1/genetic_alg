/* Auxiliary Functions cpp file for GAIM software 
 * Copyright (C) 2019  Georgios Detorakis (gdetor@protonmail.com)
 *                     Andrew Burton (ajburton@uci.edu)
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. */

// $Id$
/**
 * @file auxiliary_funs.cpp
 * Auxiliary functions for the GAIM package, encompassing helpers for arithmetic with individual fitnesses and wrappers for directory I/O.
 */
// $Log$

#include <bits/stdc++.h>
#include <sys/stat.h> 
#include <sys/types.h> 
#include <cstdint>
#include "gaim.h"


/**
 * This helper function compares the fitness of two individuals and returns true if the first 
 * argument has a smaller fitness or false otherwise.
 *
 * @param[in] x Data struct of type const individual_s.
 * @param[in] y Data struct of type const individual_s.
 * @return True if x.fitness < y.fitness, false otherwise.
 */
bool compare_fitness(const individual_s x, const individual_s y)
{
    return (x.fitness < y.fitness);
}


/**
 * This is a helper function serving as an binary operator for std:accumulate.
 * It helps add up the fitness belonging to individuals so that the cumulative 
 * fitness in the population can be computed, which is necessary for roulette-wheel selection, etc.
 *
 * @see one_generation_step()
 *
 * @param[in] x REAL_, it is the current accumulated fitness.
 * @param[in] y Data struct of individual_s type, it provides the new fitness 
 *          of an idividual.
 * @return The current accumulated fitness yielded in the accumulation (usually for the
 * whole population.)
 */
REAL_ average_fitness(REAL_ x, const individual_s y)
{
    return x + y.fitness;
}


/**
 * Checks if the given path (directory or file) exists. 
 *
 * @param[in] s const string that contains the name of the path.
 * @return True if the path exists and false otherwise.
 */
bool is_path_exist(const std::string &s)
{
    struct stat buffer;
    return (stat(s.c_str(), &buffer) == 0);
}


/**
 * Deletes a file.
 *
 * @param[in] fname const string that contains the name of the file to being
 * deleted. 
 * @return Zero in the case of successful deletion, non-zero value otherwise.
 */
int remove_file(const std::string fname)
{
    return remove(fname.c_str());
}


/**
 * Attempts to make a directory.
 *
 * @param[in] path const string that contains the name (path) of the new
 *             directory. 
 *
 * @return Zero upon successfully created directory, one otherwise.
 */
int mkdir_(const std::string &path)
{
    if (mkdir(path.c_str(), 0777) == -1) {
        std::cerr << "Error: " << strerror(errno) << std::endl;
        return 1;
    } else {
        std::cout << "Directory " << path << " created" << std::endl;
        return 0;
    }
}
