/* Selection Operators cpp file for GAIM software 
 * Copyright (C) 2019  Georgios Detorakis (gdetor@protonmail.com)
 *                     Andrew Burton (ajburton@uci.edu)
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA. */

// $Id$
/**
 * @file selection.cpp
 * Provides extra selection operators (which identify reproducing pairs to create
 * the next generation) usable by default within GAIM.
 */
// $Log$
#include "gaim.h"


/**
 * Implements a roulette-wheel selection (or fitness-proportionate selection)
 * mechanism. The fitness function observes a fitness for each individual 
 * and the share of the population's total fitness is used to associate a selection  
 * probability with each individual. 
 *
 * @param[in] population A vector of the entire population of individuals
 * @return A pair of individuals, or the two selected parents. 
 */
std::pair<individual_s, individual_s> roulette_wheel_selection(std::vector<individual_s> &population)
{
    individual_s parent1, parent2;
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_real_distribution<> uniform(0, 1);

    REAL_ prob = 0;
    static REAL_ cumulative = std::accumulate(population.begin(),
                                              population.end(),
                                              0,
                                              average_fitness);
    // First parent selection
    float r = uniform(gen);
    for (std::size_t i = 0; i < population.size(); ++i) {
        prob += population[i].fitness / cumulative;
        if (r < prob) {
            parent1 = population[i];
            break;
        }
    }

    // Second parent selection
    prob = 0;
    r = uniform(gen);
    for (std::size_t i = 0; i < population.size(); ++i) {
        prob += population[i].fitness / cumulative;
        if (r < prob) {
            parent2 = population[i];
            break;
        }
    }

    return std::make_pair(parent1, parent2);
}


/**
 * Implements a rank selection mechanism. The population is sorted based on 
 * the fitness of each individual and then a selection probability is assigned 
 * to each individual based on their rank. 
 *
 * @param[in] population A vector of the entire population of individuals
 * @return A pair of individuals, or the two selected parents. 
 */
std::pair<individual_s, individual_s> rank_selection(std::vector<individual_s> &population)
{
    REAL_ prob = 0;
    individual_s parent1, parent2;
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_real_distribution<> uniform(0, 1);

    std::sort(population.begin(), population.end(), compare_fitness);

    // First parent
    float r = uniform(gen);
    for (std::size_t i = 0; i < population.size(); ++i) {
        prob += (i+1) / population.size();
        if (r < prob) {
            parent1 = population[i];
        }
    }

    // Second parent
    prob = 0;
    r = uniform(gen);
    for (std::size_t i = 0; i < population.size(); ++i) {
        prob += (i+1) / population.size();
        if (r < prob) {
            parent2 = population[i];
        }
    }

    return std::make_pair(parent1, parent2);
}


/**
 * Implements a random selection. It randomly picks up two individuals from
 * the entire population and returns them as parents.
 *
 * @param[in] population A vector of the entire population of individuals
 * @return A pair of indivuduals, or the two selected parents. 
 */
std::pair<individual_s, individual_s> random_selection(std::vector<individual_s> &population)
{
    individual_s parent1, parent2;
    static std::random_device rd;
    static std::mt19937 gen(rd());
    static std::uniform_int_distribution<> index(0, population.size()-1);

    parent1 = population[index(gen)];
    parent2 = population[index(gen)];
    return std::make_pair(parent1, parent2);
}
