#include "gaim.h"
#include <unistd.h>


ga_parameter_s init_ga_params(void)
{
    ga_parameter_s ga_test;
    ga_test.a = -1;
    ga_test.b = 1;
    ga_test.generations = 1000;
    ga_test.population_size = 20;
    ga_test.genome_size = 2;
    ga_test.num_offsprings = 5;
    ga_test.num_replacement = 3;
    ga_test.runs = 1;
    return ga_test;
}


pr_parameter_s init_print_params(void) 
{
    pr_parameter_s pr_test;
    pr_test.where2write = "./test_data/";
    pr_test.experiment_name = "test_";
    pr_test.print_fitness = true;
    pr_test.print_average_fitness = true;
    pr_test.print_bsf = true;
    pr_test.print_best_genome = true;
    return pr_test;
}


int test_prints(void) {
    int id;
    int count = 0;
    std::string fitness("fitness_0.dat"), bsf("bsf_0.dat");
    std::string avg_fitness("average_fitness_0.dat");
    std::string genome("best_genome_0.dat"), base("./test_data/");
    ga_parameter_s ga_pms(init_ga_params());
    pr_parameter_s pr_pms(init_print_params());

    mkdir_(base);

    GA gen_alg(&ga_pms);
    gen_alg.evolve(1000, 0, &pr_pms);

    // Check if files have been created
    id = 1 - is_path_exist(base+fitness);
    check(id, "Print fitness");
    if (!id) {
        count++;
        remove_file(base+fitness);
    }

    id = 1 - is_path_exist(base+bsf);
    check(id, "Print BSF");
    if (!id) {
        count++;
        remove_file(base+bsf);
    }

    id = 1 - is_path_exist(base+avg_fitness);
    check(id, "Print Average Fitness");
    if (!id) {
        count++;
        remove_file(base+avg_fitness);
    }

    id = 1 - is_path_exist(base+genome);
    check(id, "Print Best Genome");
    if (!id) {
        count++;
        remove_file(base+genome);
    }

    rmdir(base.c_str());

    if (count == 4) {
        std::cout << "Test passed: " << count << " / 4" << std::endl;
        return 0;
    } 
    else {
        std::cout << "Test failed: " << (4 - count) << " / 4" << std::endl;
        return 1;
    }
}


int main() 
{
    test_prints();
    return 0;
}
